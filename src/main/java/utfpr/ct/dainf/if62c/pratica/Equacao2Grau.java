/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author henrique
 */
public class Equacao2Grau<T extends Number> {
    private T a;
    private T b;
    private T c;
    
    public Equacao2Grau(T a, T b, T c){
        if(a.equals(0)){
            throw new RuntimeException("Coeficiente não pode ser zero");
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public void setA(T a){
        if(a.equals(0)){
            throw new RuntimeException("Coeficiente não pode ser zero");
        }
        this.a = a;
    }

    public void setB(T b){
        this.b = b;
    }

    public void setC(T c){
        this.c = c;
    }    

    public T getA(){
        return this.a;
    }    
    
    public T getB(){
        return this.b;
    }    
    
    public T getC(){
        return this.c;
    }
    
    public double getDelta(){
        return Math.pow(this.getB().doubleValue(), 2) - (4*this.getA().doubleValue()*this.getC().doubleValue());
    }
    
    public double getRaiz1(){
        if(this.getDelta() < 0){
            throw new RuntimeException("Equação não tem solução real");
        }
        double resultado = (-this.getB().doubleValue() + Math.sqrt(this.getDelta()))/(2*this.getA().doubleValue());
        return resultado;
    }
    
    public double getRaiz2(){
        if(this.getDelta() < 0){
            throw new RuntimeException("Equação não tem solução real");
        }
        double resultado = (-this.getB().doubleValue() - Math.sqrt(this.getDelta()))/(2*this.getA().doubleValue());
        return resultado;
    }
}
