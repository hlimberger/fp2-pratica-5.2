
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author henrique
 */
public class Pratica52 {
    
    public static void main(String[] args){
        Equacao2Grau eq = new Equacao2Grau(2, 7, 5);
        double x1 = eq.getRaiz1();
        double x2 = eq.getRaiz2();
        
        System.out.println(String.format("x1: %f e x2: %f",x1,x2));
        
        eq = new Equacao2Grau(2, -4, 2);
        x1 = eq.getRaiz1();
        x2 = eq.getRaiz2();
        
        System.out.println(String.format("x1: %f e x2: %f",x1,x2));
 
        eq = new Equacao2Grau(2, 2, 2);
        if(eq.getDelta() >= 0){
            x1 = eq.getRaiz2();
            x2 = eq.getRaiz2();
            System.out.println(String.format("x1: %f e x2: %f",x1,x2));
        } else {
            System.out.println("A equação não possui raizes");
        }
        


    }
}
